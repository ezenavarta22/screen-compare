const relations = [
        {
            value: "16x9",
            label: "16 x 9 - Widescreen",
            resolutions: [
                "1920 x 1080",
                "1366 x 768"
            ]
        },
        {
            value: "16x10",
            label: "16 x 9 - Widescreen",
            resolutions: [
                "1680 x 1050"
            ]
        },
    ]

// const screen1width = document.getElementById('screen1width')
// const screen1height = document.getElementById('screen1height')
// const screen1pulgas = document.getElementById('screen1pulgas')
// const screen2width = document.getElementById('screen2width')
// const screen2height = document.getElementById('screen2height')
// const screen2pulgas = document.getElementById('screen2pulgas')

const aspectDD = document.getElementById('aspectDD')
const btnCalcular = document.getElementById('btnCalcular')


let alto1 = 0
let ancho1 = 0
let alto2 = 0
let ancho2 = 0

btnCalcular.onclick = function(){
    calculate()
    draw()
};

function draw() {
    var canvas = document.getElementById('canvas');
    if (canvas.getContext) {
        const max = Math.max(alto1, ancho1, alto2, ancho2)
        const relacion = canvas.width/max
        let anchoFinal1 = ancho1 * relacion
        let altoFinal1 = alto1 * relacion
        let anchoFinal2 = ancho2 * relacion
        let altoFinal2= alto2 * relacion
        
        var ctx = canvas.getContext('2d');
        
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        
        ctx.fillStyle = 'rgba(200, 0, 0, 0.7)';
        ctx.fillRect(0, 0, anchoFinal1, altoFinal1);

        ctx.fillStyle = 'rgba(0, 0, 200, 0.7)';
        ctx.fillRect(0, 0, anchoFinal2, altoFinal2);
    }
}

function calculate() {
    alto1 = calcularAltura(screen1width.value, screen1height.value, screen1pulgas.value)
    ancho1 = calcularAnchura(screen1width.value, screen1height.value, screen1pulgas.value)

    alto2 = calcularAltura(screen2width.value, screen2height.value, screen2pulgas.value)
    ancho2 = calcularAnchura(screen2width.value, screen2height.value, screen2pulgas.value)

    console.log(
        'alto: ' + calcularAltura(screen1width.value, screen1height.value, screen1pulgas.value) +
        ' ancho: ' + calcularAnchura(screen1width.value, screen1height.value, screen1pulgas.value)
        )
    console.log(
        'alto: ' + calcularAltura(screen2width.value, screen2height.value, screen2pulgas.value) +
        ' ancho: ' + calcularAnchura(screen2width.value, screen2height.value, screen2pulgas.value)
        )
}

function calcularAltura(PXancho, PXalto, pulgas){
    return calculoAA(PXalto, PXancho, pulgas)
}
function calcularAnchura(PXancho, PXalto, pulgas){
    return calculoAA(PXancho, PXalto, pulgas)
}

function calculoAA(foo, bar, pulgas){
    const diagonal = Math.sqrt((bar ** 2) + (foo ** 2))
    const altura = (pulgas * bar) / diagonal
    return altura
}

function calcularAspect(alto, ancho){
    return ancho / alto;
}
